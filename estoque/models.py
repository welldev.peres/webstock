from django.db import models


class Equipamento(models.Model):
    SETUP = (
        ('D', 'Desktop'),
        ('N', 'Notebook'),
        ('P', 'Particular'),
    )
    setup = models.CharField(max_length=1, choices=SETUP, blank=False, null=False,default='E')
    service_tag = models.CharField(max_length=15)
    armazenamento = models.CharField(max_length=15)
    memoria = models.CharField(max_length=5)
    processador = models.CharField(max_length=10)
    modelo = models.CharField(max_length=15)
    data_da_compra = models.DateField()
    RMM = (
        ('S', 'SIM'),
        ('N', 'NÃO')
    )
    rmm = models.CharField(max_length=1, choices=RMM, blank=False, null=False,default='N')
    def __str__(self):
        return self.service_tag

class Funcionario(models.Model):
    equipamento = models.ForeignKey(Equipamento, blank=False, null=True, on_delete=models.SET_NULL)
    nome = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    usuario = models.CharField(max_length=30)
    data_inicio = models.DateField()
    SETOR = (
        ('RH', 'RH'),
        ('Formalização', 'Formalização'),
        ('Canal Interno', 'Canal Interno'),
        ('Checagem', 'Checagem'),
        ('Cobrança', 'Cobrança'),
        ('Compliance', 'Compliance'),
        ('Juridico', 'Juridico'),
        ('Operação', 'Operação'),
        ('Service', 'Service'),
        ('Tesouraria', 'Tesouraria'),
        ('TI', 'TI'),
    )
    setor = models.CharField(max_length=15, choices=SETOR, blank=False, null=False,default='')

    def __str__(self):
        return self.nome

