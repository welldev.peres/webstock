from django.contrib import admin

from .models import Equipamento, Funcionario

class Equipamentos(admin.ModelAdmin):
    list_display = ('id','service_tag', 'modelo')
    list_display_links = ('id', 'service_tag')
    search_fields = ('modelo',)
    ordering = ['id']
    list_per_page = 25

admin.site.register(Equipamento, Equipamentos)

class Funcionarios(admin.ModelAdmin):
    list_display = ('id', 'nome', 'setor', 'data_inicio')
    list_display_links = ('nome', 'setor')
    search_fields = ('nome', )
    ordering = ['setor', 'nome']
    list_per_page = 25

admin.site.register(Funcionario, Funcionarios)

