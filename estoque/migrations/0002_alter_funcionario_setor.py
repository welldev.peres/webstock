# Generated by Django 5.0.3 on 2024-03-19 15:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('estoque', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='funcionario',
            name='setor',
            field=models.CharField(choices=[('RH', 'RH'), ('Formalização', 'Formalização'), ('Canal Interno', 'Canal Interno'), ('Checagem', 'Checagem'), ('Cobrança', 'Cobrança'), ('Compliance', 'Compliance'), ('Juridico', 'Juridico'), ('Operação', 'Operação'), ('Service', 'Service'), ('Tesouraria', 'Tesouraria'), ('TI', 'TI')], default='', max_length=15),
        ),
    ]
