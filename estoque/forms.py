from django import forms
from .models import *

class EquipamentoFormAdicionar(forms.ModelForm):
    class Meta:
        model = Equipamento
        fields = ['setup', 'service_tag', 'armazenamento', 'memoria', 'processador', 'modelo', 'data_da_compra', 'rmm']


class EquipamentoForm(forms.ModelForm):
    class Meta:
        model = Equipamento
        fields = ['setup', 'service_tag', 'armazenamento', 'memoria', 'processador', 'modelo', 'data_da_compra', 'rmm']  # Liste os campos que você deseja editar

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['service_tag'].widget.attrs['readonly'] = True


class FuncionarioForm(forms.ModelForm):
    class Meta:
        model = Funcionario
        fields = '__all__'  # Liste os campos que você deseja editar
