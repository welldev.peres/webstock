from django.urls import path
from .views import * 

urlpatterns = [
    path('', Home, name='home'),
    path('parque/equipamentos/lista_equipamento/', ListaEquipamento, name='lista_equipamento'),
    path('parque/equipamentos/add_equipamento/', AddEquipamento, name='add_equipamento'),
    path('parque/equipamentos/edit_equipamento/<int:equipamento_id>/', EditEquipamento, name='edit_equipamento'),
    path('parque/equipamentos/delete_equipamento/<int:equipamento_id>/deletar/', DeleteEquipamento, name='delete_equipamento'),
    path('parque/usuarios/lista_usuario/', ListaUsuario, name='lista_usuario'),
    path('parque/usuarios/add_usuario/', AddUsuario, name='add_usuario'),
    path('parque/usuarios/edit_usuario/<int:usuario_id>/', EditUsuario, name='edit_usuario'),
    path('parque/usuarios/delete_usuario/<int:usuario_id>/deletar/', DeleteUsuario, name='delete_usuario'),
]

