from django.shortcuts import render, get_object_or_404, redirect
from . models import *
from .forms import EquipamentoForm, FuncionarioForm, EquipamentoFormAdicionar


# ---------------------VIEWS INICIAIS HOMEPAGE E LISTA DE PARQUE -----------------.

def Home(request):
    parks = Funcionario.objects.all().order_by('setor', 'nome')
    return render(request, 'home.html', {'parks':parks})


#----------------------VIEWS DE EQUIPAMENTOS-------------------------------

def ListaEquipamento(request):
    equipamentos = Equipamento.objects.all().order_by('id')
    return render(request, 'parque/equipamentos/lista_equipamento.html/', {'equipamentos':equipamentos})
    

def AddEquipamento(request):
    if request.method == 'POST':
        form = EquipamentoFormAdicionar(request.POST)
        if form.is_valid():
            form.save()
            return redirect(ListaEquipamento)
    else:
        form = EquipamentoFormAdicionar()
    print(form)
    return render(request, 'parque/equipamentos/add_equipamento.html/', {'form':form})


def EditEquipamento(request, equipamento_id):
    equipamento = get_object_or_404(Equipamento, pk=equipamento_id)

    if request.method == 'POST':
        # Se o formulário for submetido, cria uma instância do formulário com os dados submetidos
        form = EquipamentoForm(request.POST, instance=equipamento)
        if form.is_valid():
            # Salva os dados editados
            form.save()
            return redirect (ListaEquipamento)  # Redireciona para a lista de itens após a edição
    else:
        # Se não for uma solicitação POST, cria uma instância do formulário com os dados atuais do objeto
        form = EquipamentoForm(instance=equipamento)

    return render(request, 'parque/equipamentos/edit_equipamento.html/', {'form':form})


def DeleteEquipamento(request, equipamento_id):
    equipamento = get_object_or_404(Equipamento, pk=equipamento_id)
    if request.method == 'POST':
        equipamento.delete()
        return redirect(ListaEquipamento)
    return render(request, 'parque/equipamentos/delete_equipamento.html', {'equipamento':equipamento})

#----------------------VIEWS DE USUÁRIOS---------------------------------
def ListaUsuario(request):
    usuarios = Funcionario.objects.all().order_by('setor', 'nome')
    return render(request, 'parque/usuarios/lista_usuario.html/', {'usuarios':usuarios})

def AddUsuario(request):
    if request.method == 'POST':
        form = FuncionarioFormAdicionar(request.POST)
        if form.is_valid():
            form.save()
            return redirect(ListaUsuario)
    else:
        form = FuncionarioForm()
    return render(request, 'parque/usuarios/add_usuario.html/', {'form':form})

def EditUsuario(request, funcionario_id):
    funcionario = get_object_or_404(Funcionario, pk=funcionario_id)

    if request.method == 'POST':
        form = FuncionarioForm(request.POST, instance=funcionario)
        if form.is_valid():
            form.save()
            return redirect(ListaUsuario)
    else:
        form = FuncionarioForm(instance=funcionario)

    return render(request, 'parque/usuarios/edit_usuario.html/', {'form':form})

def DeleteUsuario(request, usuario_id):
    usuario = get_object_or_404(Funcionario, pk=usuario_id)
    if request.method == 'POST':
        usuario.delete()
        return redirect(ListaUsuario)
    return render(request, 'parque/usuarios/delete_usuario.html/', {'usuario':usuario})

