# Use uma imagem base do Python
FROM python:3.10-slim

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Defina o diretório de trabalho dentro do contêiner
WORKDIR /webstock

# Copie o arquivo requirements.txt para o diretório de trabalho
COPY . .

# Instale as dependências
RUN pip install --no-cache-dir -r requirements.txt

# Copie todo o código fonte para o diretório de trabalho
COPY . .

# Exponha a porta em que a aplicação será executada (se necessário)
EXPOSE 8000

# Comando para iniciar a aplicação quando o contâiner for iniciado
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]